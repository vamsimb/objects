function keys(obj) {

  let allValues = [];

  if (typeof(obj) != "object" || Array.isArray(obj) == true) {
    return allValues;
  }

  if(typeof(obj) == "object" && obj == null) {
    return allValues;
  }

  for (var key in obj) {
    allValues.push(obj[key]);
  }

  return allValues;
}

module.exports = keys;