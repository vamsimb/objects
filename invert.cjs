function invert(obj) {
  let result = {};

  if (typeof(obj) != "object" || Array.isArray(obj) == true) {
    return result;
  }

  if(typeof(obj) == "object" && obj == null) {
    return result;
  }
  for(let key in obj) {
    result[obj[key]] = key;
  }
  return result;
}

module.exports = invert;