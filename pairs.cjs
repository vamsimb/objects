const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };


function pairs(obj) {
  let result = [];

  if (typeof(obj) != "object" || Array.isArray(obj) == true) {
    return result;
  }

  if(typeof(obj) == "object" && obj == null) {
    return result;
  }
  for(let key in obj) {
    result.push([key, obj[key]]);
  }
  return result;
}


module.exports = pairs;