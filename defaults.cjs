function defaults(lookFor, obj) {
  let result = {};

  if (typeof(obj) != "object" || Array.isArray(obj) == true) {
    return result;
  }
  if(typeof(obj) == "object" && obj == null) {
    return result;
  }
  if (lookFor.length == 0 || typeof (lookFor) != "object") {
    return obj;
  }
  for(let key in obj) {
    if (lookFor[key])  {
      result[key] = lookFor[key];
    }
    else {
      result[key] = obj[key];
    }
  }
return result;
}

module.exports = defaults;
