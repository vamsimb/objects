const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function keys(obj,number) {
  //callback function
  function callback(value, number) {
    return value+number;
  }
  
  let result = {};

  if (typeof(obj) != "object" || Array.isArray(obj) == true) {
    return result;
  }

  if(typeof(obj) == "object" && obj == null) {
    return result;
  }

  for (let key in obj) {
    result[key]=callback(obj[key], number);
  }
  return result;
}


module.exports = keys;