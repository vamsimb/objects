function keys(obj) {

  let allkeys = [];

  if (typeof(obj) != "object" || Array.isArray(obj) == true) {
    return allkeys;
  }

  if(typeof(obj) == "object" && obj == null) {
    return allkeys;
  }

  for (var key in obj) {
    allkeys.push(key);
  }

  return allkeys;
}

module.exports = keys;